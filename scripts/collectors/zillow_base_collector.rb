require 'rubygems'
require 'mechanize'
require "sequel"

  class ZillowBaseCollector

      def initialize
          @mechanize = Mechanize.new { |agent|
            agent.user_agent_alias = 'Mac Safari'
          }
          @mechanize.agent.follow_meta_refresh = true
          @mechanize.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      end

      def connect(url)
        @page = @mechanize.get(url)
      end

      def connect_to_db
            @DB = Sequel.connect(:adapter => 'mysql2', :user => 'admin', :host => 'localhost', :database => 'revas_app_development',:password=>'admin')
            puts 'Successfully connected to DB'
      end

      def read_not_parsed
        zip_codes = @DB[:zip_codes]
        @codes = zip_codes.filter(:status => 'NOT_PARSED')
        puts 'Total of Codes to be Parsed: ' + @codes.count.to_s
      end

      def update_zip_codes(zip_code, status, total)
        zip_codes = @DB[:zip_codes]
        zip_codes.where("zipcode = ?", zip_code).update(:status => 'PARSED', :total_parsed => total)
        puts 'Property Updated'
      end
      #Store the info into the property link table
      def store_record(property_link, source, latitude, longitude, address, city, state, country,  postal_code, foreclosure_status, price)
        table = @DB[:property_links]
        created_at = Time.now
        updated_at = Time.now
        table.insert(:property_url => property_link, :source => source, :address => address, :state => state, :country => country, :city=> city,
             :latitude => latitude, :longitude => longitude,  :created_at => created_at, :updated_at => updated_at,
            :foreclosure_status => foreclosure_status, :postal_code => postal_code, :price => price)

      end

      def login_zillow
        connect('http://www.zillow.com/')
        #login_opener
        link = @page.link_with(:id => "login_opener")
        @page = link.click

        #login to Zillow
        form = @page.form(:id=>"loginForm")
        form.field_with(:id=>"email").value="lbela_01@hotmail.com"
        form.field_with(:id=>"password").value="Chaduro"
        @page = form.submit

        puts 'Successfully logged in to Zillow'
      end

      def submit_search(zip_code)
        #submit code 45206
        puts 'Parsing code: ' + zip_code.to_s
        form = @page.form(:name=>"formSearchBar")
        form.field_with(:id=>"citystatezip").value=zip_code
        @page = form.submit
      end

      def parse_properties
          #collect properties
          properties = @page.css("#search-results article")
          puts 'Total Properties: ' + properties.count.to_s

            properties.each do |pro|
                  unless pro.css("a[href*='/homedetails/']").nil? || pro.css("a[href*='/homedetails/']")[0].nil?
                    property_link = 'http://www.zillow.com' + pro.css("a[href*='/homedetails/']")[0]['href']
                    latitude = pro['data-latitude']
                    longitude = pro['data-longitude']
                    address = pro.css("span[itemprop='streetAddress']").text
                    city = pro.css("span[itemprop='addressLocality']").text
                    state = pro.css("span[itemprop='addressRegion']").text
                    postal_code = pro.css("span[itemprop='postalCode']").text
                    foreclosure_status = pro['data-sgapt']
                    price = pro.css("span[class='zsg-photo-card-price']").text

                    puts 'Property Url: ' + property_link
                    puts 'Longitude: ' + longitude
                    puts 'Latitude: ' + latitude
                    puts 'Address: ' + address
                    puts 'City: ' + city
                    puts 'State: ' + state
                    puts 'Postal Code: ' + postal_code
                    puts 'Status: ' + foreclosure_status
                    puts 'Price: ' + price
                    #handle exception
                    begin
                      store_record(property_link, 'zillow', latitude, longitude, address, city, state, 'US',  postal_code, foreclosure_status , price )
                      @properties_collected += 1
                    rescue

                  end
                end
          end

          #Pagination
          @pagination = @page.search("li[class='zsg-pagination-next'] a:first-child")

          if (!@pagination.nil? && !@pagination[0].nil?)
            #puts 'http://www.zillow.com' + @pagination[0]['href']
            loop_pagination('http://www.zillow.com' + @pagination[0]['href'])
          end


      end

      def loop_pagination(url)
        puts "Connecting to: " + url
        connect(url)
        parse_properties()
      end

      #Main method to parse the properties
      def parse
        #connect to the db
        connect_to_db()
        #Read Zip Codes Not Parsed
        read_not_parsed()
        #Login zillow
        login_zillow()

        #Parsing available properties
        @codes.each do |code|
          @properties_collected = 0
          connect('http://www.zillow.com/')
          submit_search(code[:zipcode])
          parse_properties();
          puts 'Total Properties Collected: ' + @properties_collected.to_s

          #Updating tables zip codes :status and Total Properties
          update_zip_codes(code[:zipcode], 'PARSED', @properties_collected)
        end

     end

  end

zillow = ZillowBaseCollector.new()
#zillow.connect_to_db()
#zillow.read_not_parsed()
#zillow.login_zillow()
zillow.parse()

#zillow.submit_search("45201")
#zillow.parse_properties();


#"45206"

require 'rubygems'
require 'mechanize'
require "sequel"

class CincinnatiZipCollector
  def initialize
      @mechanize = Mechanize.new { |agent|
        agent.user_agent_alias = 'Mac Safari'
      }
      @mechanize.agent.follow_meta_refresh = true
      @mechanize.agent.http.verify_mode = OpenSSL::SSL::VERIFY_NONE
  end

  def connect_to_db
        @DB = Sequel.connect(:adapter => 'mysql2', :user => 'admin', :host => 'localhost', :database => 'revas_app_development',:password=>'admin')
  end

  def store_record(zip_code, city, state, county, country, latitude, longitude)
    table = @DB[:zip_codes]
    created_at = Time.now
    updated_at = Time.now
    table.insert(:zipcode => zip_code, :city => city, :state => state, :country => country,
        :county => county, :latitude => latitude, :longitude => longitude,  :created_at => created_at, :updated_at => updated_at)

  end
  def connect()
    @page = @mechanize.get('http://cincinnati.areaconnect.com/zip2.htm?city=Cincinnati')
  end

  def parse_zip_codes
      #collectzip codes
      connect()
      connect_to_db()
      zip_codes = @page.css("div[class='row']")
      puts 'Total Zip Codes Available: ' + zip_codes.count.to_s

        zip_codes.each do |zip|
                zip_code = zip.css('div[class="block zip1"]').text.strip
                city = zip.css('div[class="block zip2"]').text.strip
                county = zip.css('div[class="block zip5"]').text.strip
                state = zip.css('div[class="block zip3"]').text.strip
                latitude = zip.css('div[class="block zip6"]').text.strip
                longitude = zip.css('div[class="block zip7"]').text.strip
                puts 'Zip Code: ' + zip_code
                puts 'City: ' + city
                puts 'County: ' + county
                puts 'State: ' + state
                puts 'latitude: ' + latitude
                puts 'longitude: ' + longitude
                puts 'Country: ' + 'US'
                puts ''

                store_record(zip_code, city, state, county, 'US', latitude, longitude)
            end
      end
end

zip_collector = CincinnatiZipCollector.new()
zip_collector.parse_zip_codes()

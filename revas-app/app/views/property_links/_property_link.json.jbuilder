json.extract! property_link, :id, :created_at, :updated_at
json.url property_link_url(property_link, format: :json)
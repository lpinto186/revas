class AdminController < ApplicationController
  def index
    if user_signed_in?
      redirect_to "/admin/dashboard"
    else
      redirect_to new_user_session_path
    end
  end
  def dashboard
  end
end

class PropertyLinksController < ApplicationController
  before_action :set_property_link, only: [:show, :edit, :update, :destroy]

  # GET /property_links
  # GET /property_links.json
  def index
    @property_links = PropertyLink.all
  end

  # GET /property_links/1
  # GET /property_links/1.json
  def show
  end

  # GET /property_links/new
  def new
    @property_link = PropertyLink.new
  end

  # GET /property_links/1/edit
  def edit
  end

  # POST /property_links
  # POST /property_links.json
  def create
    @property_link = PropertyLink.new(property_link_params)

    respond_to do |format|
      if @property_link.save
        format.html { redirect_to @property_link, notice: 'Property link was successfully created.' }
        format.json { render :show, status: :created, location: @property_link }
      else
        format.html { render :new }
        format.json { render json: @property_link.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /property_links/1
  # PATCH/PUT /property_links/1.json
  def update
    respond_to do |format|
      if @property_link.update(property_link_params)
        format.html { redirect_to @property_link, notice: 'Property link was successfully updated.' }
        format.json { render :show, status: :ok, location: @property_link }
      else
        format.html { render :edit }
        format.json { render json: @property_link.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /property_links/1
  # DELETE /property_links/1.json
  def destroy
    @property_link.destroy
    respond_to do |format|
      format.html { redirect_to property_links_url, notice: 'Property link was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_property_link
      @property_link = PropertyLink.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def property_link_params
      params.fetch(:property_link, {})
    end
end

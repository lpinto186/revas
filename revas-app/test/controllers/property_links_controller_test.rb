require 'test_helper'

class PropertyLinksControllerTest < ActionDispatch::IntegrationTest
  setup do
    @property_link = property_links(:one)
  end

  test "should get index" do
    get property_links_url
    assert_response :success
  end

  test "should get new" do
    get new_property_link_url
    assert_response :success
  end

  test "should create property_link" do
    assert_difference('PropertyLink.count') do
      post property_links_url, params: { property_link: {  } }
    end

    assert_redirected_to property_link_url(PropertyLink.last)
  end

  test "should show property_link" do
    get property_link_url(@property_link)
    assert_response :success
  end

  test "should get edit" do
    get edit_property_link_url(@property_link)
    assert_response :success
  end

  test "should update property_link" do
    patch property_link_url(@property_link), params: { property_link: {  } }
    assert_redirected_to property_link_url(@property_link)
  end

  test "should destroy property_link" do
    assert_difference('PropertyLink.count', -1) do
      delete property_link_url(@property_link)
    end

    assert_redirected_to property_links_url
  end
end

class CreateZipCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :zip_codes do |t|
      t.string :zipcode
      t.string :city
      t.string :county
      t.string :state
      t.string :country

      t.timestamps
    end
  end
end

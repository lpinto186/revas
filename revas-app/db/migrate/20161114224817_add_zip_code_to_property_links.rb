class AddZipCodeToPropertyLinks < ActiveRecord::Migration[5.0]
  def change
    add_index :property_links, :zip_code_id
  end
end

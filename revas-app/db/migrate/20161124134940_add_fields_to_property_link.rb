class AddFieldsToPropertyLink < ActiveRecord::Migration[5.0]
  def change
    add_column :property_links, :address, :string
    add_column :property_links, :city, :string
    add_column :property_links, :state, :string
    add_column :property_links, :country, :string
    add_column :property_links, :postal_code, :string
    add_column :property_links, :foreclosure_status, :string
    add_column :property_links, :price, :float
  end
end

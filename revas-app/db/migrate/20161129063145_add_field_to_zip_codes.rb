class AddFieldToZipCodes < ActiveRecord::Migration[5.0]
  def change
    add_column :zip_codes, :total_parsed, :integer
  end
end

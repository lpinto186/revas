class AaddFieldsToZipCodes < ActiveRecord::Migration[5.0]
  def change
     add_column :zip_codes, :latitude, :string
     add_column :zip_codes, :longitude, :string
  end
end

class RemoveColumnsFromPropertyLinks < ActiveRecord::Migration[5.0]
  def change
    remove_column :property_links, :date 
  end
end

class CreatePropertyLinks < ActiveRecord::Migration[5.0]
  def change
    create_table :property_links do |t|
      t.string :property_url
      t.string :source
      t.datetime :date
      t.string :latitude
      t.string :longitude
      t.integer :zip_code_id

      t.timestamps
    end
  end
end

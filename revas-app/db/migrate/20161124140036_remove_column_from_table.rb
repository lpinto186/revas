class RemoveColumnFromTable < ActiveRecord::Migration[5.0]
  def change
    remove_column :property_links, :zip_code_id, :integer
  end
end

Rails.application.routes.draw do
  resources :sources
  resources :zip_codes
  devise_for :users
  resources :property_links
  get 'admin/index'
  get 'admin/dashboard'

  root 'admin#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
